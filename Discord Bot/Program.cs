﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.Commands;
using Discord.Net.Providers.WS4Net;
using Discord.WebSocket;
using System.IO;
using System.Diagnostics;

namespace Discord_Bot
{
	public class Program
	{
		string rulesetName;
		Random rand;
		private readonly DiscordSocketClient _client;

		private readonly IServiceCollection _map = new ServiceCollection();
		private readonly CommandService _commands = new CommandService();

		private IServiceProvider _services;

		public static void Main(string[] args)
			=> new Program().MainAsync().GetAwaiter().GetResult();

		private Program()
		{
			_client = new DiscordSocketClient(new DiscordSocketConfig
			{
				WebSocketProvider = WS4NetProvider.Instance,
				LogLevel = LogSeverity.Info,
			});
		}

		public async Task MainAsync()
		{
			rand = new Random();

			_client.Log += Log;

			await InitCommands();

			string token = "NTE1Mzg0MTYxMjQzMTAzMjMy.DtpaMg.pXpD2SoqFi-jwjCiXyLv8W8VWGQ";
			await _client.LoginAsync(TokenType.Bot, token);
			await _client.StartAsync();

			ConnectionState previousState = _client.ConnectionState;
			Stopwatch timeout = new Stopwatch();
			TimeSpan timeoutLength = new TimeSpan(0, 2, 0);
			while (true)
			{
				try
				{
					if (Console.ReadLine() == "restart")
					{
						if (_client.ConnectionState == ConnectionState.Connecting)
						{
							await _client.LogoutAsync();
							await _client.LoginAsync(TokenType.Bot, token);
						}
						else
						{
							await _client.StopAsync();
						}
						await _client.StartAsync();
					}

					if (previousState == _client.ConnectionState && previousState != ConnectionState.Connected)
					{
						if (!timeout.IsRunning) timeout.Start();
						if (timeout.Elapsed > timeoutLength)
						{
							if (_client.ConnectionState == ConnectionState.Connecting)
							{
								await _client.LogoutAsync();
								await _client.LoginAsync(TokenType.Bot, token);
							}
							else
							{
								await _client.StopAsync();
							}
							await _client.StartAsync();
							Console.ForegroundColor = ConsoleColor.Red;
							Console.WriteLine("Bot timed out. Forced reconnection.");
							Console.ForegroundColor = ConsoleColor.White;
							timeout.Reset();
						}
					}
					else if (timeout.IsRunning)
					{
						timeout.Reset();
					}
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message + "\r\n" + e.StackTrace);
				}
				previousState = _client.ConnectionState;
			}
		}

		public async Task InitCommands()
		{
			_services = _map.BuildServiceProvider();

			await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);

			_client.MessageReceived += MessageRecieved;
			await _client.SetGameAsync($"{RuleSet.name} | !help");
			rulesetName = RuleSet.name;
		}

		public async Task MessageRecieved(SocketMessage messageParam)
		{
			SocketUserMessage message = messageParam as SocketUserMessage;
			if (message == null) return;

			int prefixPos = 0;

			if (message.HasCharPrefix('!', ref prefixPos))
			{
				SocketCommandContext context = new SocketCommandContext(_client, message);

				if (!Directory.Exists("Guilds"))
				{
					Directory.CreateDirectory("Guilds");
				}

				if (!Directory.Exists($"Guilds/{context.Guild.Id}"))
				{
					Directory.CreateDirectory($"Guilds/{context.Guild.Id}");
				}

				var result = _commands.ExecuteAsync(context, prefixPos, _services);
				if (RuleSet.name != rulesetName)
				{
					await _client.SetGameAsync($"{RuleSet.name} | !help");
					rulesetName = RuleSet.name;
				}
			}
		}

		private static Task Log(LogMessage message)
		{
			var cc = Console.ForegroundColor;
			switch (message.Severity)
			{
				case LogSeverity.Critical:
				case LogSeverity.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case LogSeverity.Warning:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				case LogSeverity.Info:
					Console.ForegroundColor = ConsoleColor.White;
					break;
				case LogSeverity.Verbose:
				case LogSeverity.Debug:
					Console.ForegroundColor = ConsoleColor.DarkGray;
					break;
			}
			Console.WriteLine($"{DateTime.Now,-19} [{message.Severity,8}] {message.Source}: {message.Message}");
			Console.ForegroundColor = cc;

			return Task.CompletedTask;
		}
	}
}

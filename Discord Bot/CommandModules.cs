﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using System.Reflection;

namespace Discord_Bot
{
	public class RollModule : ModuleBase
	{
		[Command("roll"), Summary("Rolls some dice.")]
		public async Task Roll([Remainder, Summary("<die size> <number to roll>")] string arguments)
		{
			string reply = "";
			string[] words = arguments.Split(' ');
			Random rand = new Random();
			var userInfo = Context.User;

			if (words.Length >= 2)
			{
				int die;
				if (words[0].StartsWith("d", true, null) && int.TryParse(words[0].Substring(1), out die))
				{
					int roll;
					if (int.TryParse(words[1], out roll))
					{
						int num = 0;
						List<int> nums = new List<int>();

						for (int i = 0; i < roll; i++)
						{
							int temp = rand.Next(1, die + 1);
							num += temp;
							nums.Add(temp);
						}
						reply = $"{userInfo.Mention} rolled {words[1]} {words[0].ToLower()}s for {num} : {string.Join(", ", nums)}.";
					}
					else reply = $"{userInfo.Mention} Please ensure you are trying to roll a number of dice and not some strang non-number.";
				}
				else reply = $"{userInfo.Mention} Please ensure that the die you are trying to roll starts with \"d\" and is actually a number.";
			}
			else reply = $"{userInfo.Mention} ```!roll <die size> <number to roll>```";
			await ReplyAsync(reply);
		}

		[Command("rollGroup"), Summary("Rolls a group of dice.")]
		public async Task RollGroup([Remainder, Summary("<die size> <number of groups> <size of group>")] string arguments)
		{
			string reply = "";
			string[] words = arguments.Split(' ');
			Random rand = new Random();
			var userInfo = Context.User;

			if (words.Length >= 2)
			{
				int die;
				if (words[0].StartsWith("d", true, null) && int.TryParse(words[0].Substring(1), out die))
				{
					int roll;
					if (int.TryParse(words[1], out roll))
					{
						int num = 0, group = 0;
						List<int> nums = new List<int>();

						if (int.TryParse(words[2], out group))
						{
							for (int i = 0; i < roll; i++)
							{
								num = 0;
								for (int k = 0; k < group; k++)
								{
									num += rand.Next(1, die + 1);
								}
								nums.Add(num);
							}
							reply = $"{userInfo.Mention} rolled {words[1]} groups of {words[2]} {words[0].ToLower()} for {string.Join(", ", nums)}.";
						}
						else reply = $"{userInfo.Mention} Please ensure that you are trying to roll a group of dice and not some strange non-mathematical group.";
					}
					else reply = $"{userInfo.Mention} Please ensure that you are trying to roll a number of dice and not some strang non-number.";
				}
				else reply = $"{userInfo.Mention} Please ensure that the die you are trying to roll starts with \"d\" and is actually a number.";
			}
			else reply = $"{userInfo.Mention} ```!roll <die size> <number of groups> <size of group>```";
			await ReplyAsync(reply);
		}
	}

	public static class DataMemory
	{
		public static Dictionary<ulong, List<string>> userStatPriorities = new Dictionary<ulong, List<string>>();
		public static Dictionary<ulong, string> selectedCharacters = new Dictionary<ulong, string>();
	}

	public class RolePlayModule : ModuleBase
	{
		Regex newLineSplit = new Regex("\r\n");
		List<EmbedBuilder> helpEmbeds = new List<EmbedBuilder>();

		public RolePlayModule()
		{
			EmbedBuilder diceBuilder = new EmbedBuilder();
			diceBuilder.Color = Color.Purple;

			EmbedFooterBuilder dicePageFooter = new EmbedFooterBuilder();

			string rolePlayGroupString = GenerateHelpGroup(typeof(RolePlayModule));
			List<string> rolePlayGroupStrings = new List<string>();
			rolePlayGroupStrings.Add("");
			string[] splitCommandDescriptions = rolePlayGroupString.Split('\n');

			foreach (string commandDescription in splitCommandDescriptions)
			{
				if (rolePlayGroupStrings.Last().Length + commandDescription.Length > 1024) rolePlayGroupStrings.Add("");
				rolePlayGroupStrings[rolePlayGroupStrings.Count - 1] += commandDescription;
			}
			rolePlayGroupStrings.ForEach(t => t += '\n');

			for (int i = 0; i < rolePlayGroupStrings.Count; i++)
			{
				EmbedBuilder builder = new EmbedBuilder();
				builder.Color = Color.Purple;

				EmbedFieldBuilder rolePlayField = new EmbedFieldBuilder();
				rolePlayField.Name = $"Roleplaying {i + 1}:";
				rolePlayField.Value = rolePlayGroupStrings[i];
				builder.AddField(rolePlayField);

				EmbedFooterBuilder footer = new EmbedFooterBuilder();
				footer.Text = $"Page {i + 1}";
				builder.Footer = footer;

				helpEmbeds.Add(builder);
			}

			EmbedFieldBuilder diceField = new EmbedFieldBuilder();
			diceField.Name = "Dice Rolling:";
			diceField.Value = GenerateHelpGroup(typeof(RollModule));
			diceBuilder.AddField(diceField);
			dicePageFooter.Text = $"Page {rolePlayGroupStrings.Count + 1}";
			diceBuilder.Footer = dicePageFooter;

			helpEmbeds.Add(diceBuilder);
		}

		[Command("help"), Summary("Shows the help list.")]
		public async Task Help()
		{
			var userInfo = Context.User;
			IUserMessage reply = await ReplyAsync(embed: helpEmbeds[0].WithAuthor(Context.Client.CurrentUser).Build());

			await reply.AddReactionAsync(new Emoji("➡"));
			Task.Run(() => HelpReactionEvent(reply, Context.User));
		}

		private async Task HelpReactionEvent(IUserMessage message, IUser user)
		{
			int helpPage = 0;
			TimeSpan endTime = new TimeSpan(0, 30, 0);
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();
			while (stopWatch.Elapsed < endTime)
			{
				IAsyncEnumerable<IReadOnlyCollection<IUser>> reactedLeftUsers = message.GetReactionUsersAsync(new Emoji("⬅️"), 100),
					reactedRightUsers = message.GetReactionUsersAsync(new Emoji("➡"), 100);
				bool callingUserReactedLeft = await reactedLeftUsers?.All(t => { return t.Where(l => { return l.Id == user.Id; }).Count() > 0; }),
					callingUserReactedRight = await reactedRightUsers?.All(t => { return t.Where(l => { return l.Id == user.Id; }).Count() > 0; });
				if (callingUserReactedLeft || callingUserReactedRight)
				{
					await message.RemoveAllReactionsAsync();

					if (callingUserReactedLeft && helpPage > 0)
					{
						await message.ModifyAsync(t => t.Embed = helpEmbeds[--helpPage].WithAuthor(Context.Client.CurrentUser).Build());
					}
					else if (callingUserReactedRight && helpPage < helpEmbeds.Count - 1)
					{
						await message.ModifyAsync(t => t.Embed = helpEmbeds[++helpPage].WithAuthor(Context.Client.CurrentUser).Build());
					}

					if (helpPage > 0)
					{
						await message.AddReactionAsync(new Emoji("⬅️"));
					}
					if (helpPage < helpEmbeds.Count - 1)
					{
						await message.AddReactionAsync(new Emoji("➡"));
					}
				}
			}
			stopWatch.Stop();
		}

		public string GenerateHelpGroup(Type type)
		{
			string helpGroup = "";
			foreach (MethodInfo method in type.GetMethods())
			{
				CommandAttribute attb = method.GetCustomAttribute<CommandAttribute>();
				System.Reflection.ParameterInfo[] parameters = method.GetParameters();
				string parameterText = "";
				if (parameters.Length > 0)
				{
					parameterText = $" {parameters[0].GetCustomAttribute<SummaryAttribute>()?.Text}";
				}

				if (attb == null)
				{
					continue;
				}

				helpGroup += $"`!{attb.Text}{parameterText}` - {method.GetCustomAttribute<SummaryAttribute>().Text}\r\n";
			}
			return helpGroup;
		}

		[Command("loadVersion"), Summary("Loads a version from the Versions folder.")]
		public async Task LoadVersion([Remainder, Summary("<version name>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = $"{userInfo.Mention} Version loaded successfully.";

			if (File.Exists($"Versions/{arguments.ToLower()}.xml"))
			{
				RuleSet.LoadRuleSet(arguments.ToLower());
			}
			else
			{
				reply = $"{userInfo.Mention} Version file not found.";
			}

			await ReplyAsync(reply);
		}

		[Command("generate"), Summary("Generates a character backstory.")]
		public async Task GenerateBackstory()
		{
			var userInfo = Context.User;
			string[] adjective, race, dClass, location, backstory, weapon;
			Random rand = new Random();

			StreamReader reader = new StreamReader("Generator/Adjectives.txt");
			adjective = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			reader = new StreamReader("Generator/Races.txt");
			race = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			reader = new StreamReader("Generator/Classes.txt");
			dClass = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			reader = new StreamReader("Generator/Locations.txt");
			location = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			reader = new StreamReader("Generator/Backstories.txt");
			backstory = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			reader = new StreamReader("Generator/Weapons.txt");
			weapon = newLineSplit.Split(reader.ReadToEnd());
			reader.Close();

			char[] vowels = { 'A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'Y', 'y' };
			string chosenAdjective = adjective[rand.Next(adjective.Length)],
				article = vowels.Contains(chosenAdjective[0]) ? "an" : "a";

			string chosenWeapon = weapon[rand.Next(weapon.Length)],
				weaponArticle = vowels.Contains(chosenWeapon[0]) ? "an" : "a";

			await ReplyAsync($"{userInfo.Mention} should try playing as {article} {chosenAdjective} {race[rand.Next(race.Length)]} {dClass[rand.Next(dClass.Length)]} from {location[rand.Next(location.Length)]} who {backstory[rand.Next(backstory.Length)]}{(rand.Next(0, 3) != 0 ? "." : " and uses " + weaponArticle + " " + chosenWeapon + ".")}");
		}

		[Command("create"), Summary("Creates a player file using the user's username as the character name.")]
		public async Task CreateFile()
		{
			await CreateFileArguments(Context.User.Username);
		}

		[Command("create"), Summary("Creates a player file with the specified character name.")]
		public async Task CreateFileArguments([Remainder, Summary("<character name>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = $"{userInfo.Mention} File creation success.";

			if (RuleSet.Stats != null)
			{
				if (!File.Exists($"Characters/{userInfo.Id}/{arguments}.xml"))
				{
					try
					{
						XmlDocument document = new XmlDocument();
						XmlNode root = document.CreateElement("Character");
						document.AppendChild(root);
						root.Attributes.Append(document.CreateAttribute("name")).Value = arguments;
						root.AppendChild(document.CreateElement("Classes"));
						if (!Directory.Exists($"Characters/{userInfo.Id}"))
							Directory.CreateDirectory($"Characters/{userInfo.Id}");
						document.Save($"Characters/{userInfo.Id}/{arguments}.xml");
						if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id)) DataMemory.selectedCharacters.Remove(userInfo.Id);
						DataMemory.selectedCharacters.Add(userInfo.Id, arguments);
					}
					catch (Exception e)
					{
						reply = $"{userInfo.Mention} File creation failure. {e.Message}";
					}
				}
				else
				{
					reply = $"{userInfo.Mention} Character already exists please delete character before attempting to create a new one.";
				}
			}
			else
			{
				reply = "A ruleset must be loaded for a character to be created.";
			}
			await ReplyAsync(reply);
		}

		[Command("select"), Summary("Select a player character by name. Is limited to characters made by the user.")]
		public async Task Select([Remainder, Summary("<character name>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = userInfo.Mention;

			if (RuleSet.Races != null)
			{
				if (File.Exists($"Characters/{userInfo.Id}/{arguments}.xml"))
				{
					if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
						DataMemory.selectedCharacters.Remove(userInfo.Id);
					DataMemory.selectedCharacters.Add(userInfo.Id, arguments);
					reply += $" Successfully selected character `{arguments}`.";
				}
				else reply += $" Could not find character `{arguments}`. Please check spelling to ensure you have the correct name.";
			}
			else reply = "A rulest must be loaded for a character to be selected.";

			await ReplyAsync(reply);
		}

		[Command("listCharacters"), Summary("List all characters available to user.")]
		public async Task ListCharacters()
		{
			var userInfo = Context.User;

			if (Directory.Exists($"Characters/{userInfo.Id}") && Directory.GetFiles($"Characters/{userInfo.Id}").Length > 0)
			{
				EmbedBuilder builder = new EmbedBuilder();
				EmbedFieldBuilder characterField = new EmbedFieldBuilder();

				string[] characterFileNames = Directory.GetFiles($"Characters/{userInfo.Id}");
				string fieldValue = "";

				foreach (string characterFileName in characterFileNames)
				{
					fieldValue += $"\r\n{characterFileName.Split('.')[0].Split('\\')[1]}";
				}
				builder.AddField(characterField.WithName($"{userInfo.Username}'s Characters").WithValue(fieldValue));
				await ReplyAsync(embed: builder.Build());
			}
		}

		[Command("delete"), Summary("Deletes the player's currently selected character. Be sure you really want to. Aborts after 15 seconds of waiting.")]
		public async Task Delete()
		{
			var userInfo = Context.User;
			string reply = userInfo.Mention;

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				await DeleteArguments(DataMemory.selectedCharacters[userInfo.Id]);
			}
			else
			{
				reply += " A character must be selected for use. Select a character using `!select`.";
				await ReplyAsync(reply);
			}
		}

		[Command("delete"), Summary("Deletes a specified character. Be sure you really want to. Aborts after 15 seconds of waiting.")]
		public async Task DeleteArguments([Remainder, Summary("<character name>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = userInfo.Mention;

			if (File.Exists($"Characters/{userInfo.Id}/{arguments}.xml"))
			{
				await Context.Channel.SendMessageAsync($"{userInfo.Mention} Are you sure that you want to delete your character? Message yes within 15 seconds to delete or anything else to abort.");
				Stopwatch timeout = new Stopwatch();
				timeout.Start();

				while (timeout.ElapsedMilliseconds < 15000)
				{
					IMessage message = (await Context.Channel.GetMessagesAsync(1).FlattenAsync()).First();
					if (message.Author != userInfo) continue;

					if (message.Content.ToLower().StartsWith("yes"))
					{
						File.Delete($"Characters/{userInfo.Id}/{arguments}.xml");
						if (DataMemory.selectedCharacters.Contains(new KeyValuePair<ulong, string>(userInfo.Id, arguments)))
							DataMemory.selectedCharacters.Remove(userInfo.Id);
						reply += " Character successfully deleted.";
						break;
					}
					else break;
				}

				timeout.Stop();
				if (reply == userInfo.Mention)
				{
					if (timeout.ElapsedMilliseconds >= 15000) reply += " Timeout expired.";
					reply += " Deletion aborted.";
				}
			}
			else reply += " No character file found. Deletion unnecessary.";
			await ReplyAsync(reply);
		}

		[Command("rollStats"), Summary("Rolls the character's stats according to the rules set forward in the rule set.")]
		public async Task RollStats()
		{
			var userInfo = Context.User;
			string reply = $"{userInfo.Mention} Stats rolled and stored successfully.";

			if (RuleSet.Stats != null && DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();
				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNode root = document.SelectSingleNode("/Character");
				XmlNodeList oldNodes = root.SelectNodes("/Character/BaseStats");
				if (oldNodes.Count > 0) root.RemoveChild(oldNodes[0]);
				XmlNode baseStatsElement = document.CreateElement("BaseStats");
				root.AppendChild(baseStatsElement);
				Random rand = new Random();
				if (!DataMemory.userStatPriorities.ContainsKey(userInfo.Id))
				{
					foreach (string stat in RuleSet.Stats)
					{
						XmlNode statNode = document.CreateElement(stat);
						List<int> rolls = new List<int>();
						for (int i = 0; i < RuleSet.statRolls; i++)
						{
							rolls.Add(rand.Next(0, RuleSet.statDie) + 1);
						}
						rolls.Sort();
						rolls.RemoveAt(0);
						statNode.InnerText = rolls.Sum().ToString();

						baseStatsElement.AppendChild(statNode);
					}
				}
				else
				{
					List<int> statPool = new List<int>(), rolls;
					for (int i = 0; i < DataMemory.userStatPriorities[userInfo.Id].Count; i++)
					{
						rolls = new List<int>();
						for (int j = 0; j < RuleSet.statRolls; j++)
						{
							rolls.Add(rand.Next(0, RuleSet.statDie) + 1);
						}
						rolls.Sort();
						rolls.RemoveAt(0);
						statPool.Add(rolls.Sum());
					}
					statPool = statPool.OrderByDescending(t =>
					{
						return t;
					}).ToList();

					for (int i = 0; i < DataMemory.userStatPriorities[userInfo.Id].Count; i++)
					{
						XmlNode statNode = document.CreateElement(DataMemory.userStatPriorities[userInfo.Id][i]);
						statNode.InnerText = statPool[i].ToString();
						baseStatsElement.AppendChild(statNode);
					}
				}
				FlattenStatIncreases(Context, document);
				document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
			}
			else if (!DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
			}
			else
			{
				reply = $"{userInfo.Mention} A version must be loaded with the \"!loadVersion\" command.";
			}
			await ReplyAsync(reply);
		}

		[Command("rollStats"), Summary("Roll the character's stats according to a given priority.")]
		public async Task RollPriorityStats([Remainder, Summary("<stats prioritized by order and separated by ' '>")] string arguments)
		{
			var userInfo = Context.User;
			List<string> statPriorities = arguments.Split(' ').ToList();

			if (RuleSet.Stats != null)
			{
				if (statPriorities.Count == RuleSet.Stats.Count)
				{
					bool statMatch = true;
					foreach (string stat in statPriorities)
					{
						if (!RuleSet.Stats.Contains(stat))
						{
							statMatch = false;
						}
					}

					if (statMatch)
					{
						if (DataMemory.userStatPriorities.ContainsKey(userInfo.Id)) DataMemory.userStatPriorities.Remove(userInfo.Id);
						DataMemory.userStatPriorities.Add(userInfo.Id, statPriorities);
						await RollStats();
					}
					else
					{
						await ReplyAsync($"{userInfo.Mention} Incorrect priority format. Please refer to the `!listStats` command for spelling.");
					}
				}
			}
			else
			{
				await ReplyAsync($"{userInfo.Mention} A version must be loaded with the `!loadVersion` command.");
			}
		}

		private void FlattenStatIncreases(ICommandContext context, XmlDocument document)
		{
			XmlNode root = document.SelectSingleNode("/Character");
			XmlNodeList oldNodes = root.SelectNodes("/Character/Stats");
			if (oldNodes.Count > 0) root.RemoveChild(oldNodes[0]);
			XmlNode statsElement = document.CreateElement("Stats");
			root.AppendChild(statsElement);

			XmlNodeList baseStatsNodeList = root.SelectNodes("/Character/BaseStats");
			if (baseStatsNodeList.Count > 0)
			{
				foreach (XmlNode baseStatNode in baseStatsNodeList[0])
				{
					XmlNode statNode = document.CreateElement(baseStatNode.LocalName);
					int stat = int.Parse(baseStatNode.InnerText);
					foreach (XmlAttribute characterAttribute in root.Attributes)
					{
						if (characterAttribute.LocalName == "race" && RuleSet.RacesAndSubraces[characterAttribute.Value].StatIncreases.ContainsKey(baseStatNode.LocalName))
						{
							stat += RuleSet.RacesAndSubraces[characterAttribute.Value].StatIncreases[baseStatNode.LocalName];
						}
					}
					statNode.InnerText = stat.ToString();
					statsElement.AppendChild(statNode);
				}
			}
		}

		[Command("listStats"), Summary("Lists the stats available in the loaded ruleset.")]
		public async Task ListStats()
		{
			if (RuleSet.Stats != null)
			{
				EmbedBuilder builder = new EmbedBuilder();
				string fieldValue = "";

				for (int i = 0; i < RuleSet.Stats.Count; i++)
				{
					fieldValue += $"\r\n{RuleSet.Stats[i]}";
				}
				EmbedFieldBuilder field = new EmbedFieldBuilder();
				builder.AddField(field.WithName("Stats").WithValue(fieldValue));
				await ReplyAsync(embed: builder.Build());
			}
			else
			{
				await ReplyAsync($"{Context.User.Mention} A version must be loaded with the `!loadVersion` command.");
			}
		}

		[Command("setRace"), Summary("Sets race of character.")]
		public async Task SetRace([Remainder, Summary("<race>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = $"{userInfo.Mention} Race set successfully.";

			if (RuleSet.Races != null)
			{
				if (RuleSet.RacesAndSubraces.ContainsKey(arguments))
				{
					if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
					{
						try
						{
							Race race = RuleSet.RacesAndSubraces[arguments];

							XmlDocument document = new XmlDocument();
							document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
							XmlNode root = document.SelectSingleNode("/Character");
							XmlAttribute raceAttribute = root.Attributes["race"];
							if (raceAttribute == null) root.Attributes.Append(document.CreateAttribute("race")).Value = arguments;
							else raceAttribute.Value = arguments;
							FlattenStatIncreases(Context, document);

							XmlNode sizeNode = root.SelectSingleNode("Size");
							if (sizeNode == null) root.AppendChild(document.CreateElement("Size")).InnerText = race.Size;
							else sizeNode.InnerText = race.Size;

							XmlNode speedNode = root.SelectSingleNode("Speed");
							if (speedNode == null) root.AppendChild(document.CreateElement("Speed")).InnerText = $"{race.Speed}";
							else speedNode.InnerText = $"{race.Speed}";

							if (race.SavingThrows.Count > 0)
							{
								XmlNode savingThrowsNode = root.SelectSingleNode("RacialSavingThrows");
								if (savingThrowsNode == null) savingThrowsNode = root.AppendChild(document.CreateElement("RacialSavingThrows"));
								else savingThrowsNode.RemoveAll();

								foreach (KeyValuePair<string, string> savingThrow in race.SavingThrows)
								{
									savingThrowsNode.AppendChild(document.CreateElement(savingThrow.Key)).InnerText = savingThrow.Value;
								}
							}

							if (race.Resistances.Count > 0)
							{
								XmlNode resitancesNode = root.SelectSingleNode("RacialResistances");
								if (resitancesNode == null) resitancesNode = root.AppendChild(document.CreateElement("RacialResistances"));
								else resitancesNode.RemoveAll();

								foreach (string resistance in race.Resistances)
								{
									resitancesNode.AppendChild(document.CreateElement("Resistance")).InnerText = resistance;
								}
							}

							if (race.Proficiencies.Count > 0)
							{
								XmlNode proficienciesNode = root.SelectSingleNode("RacialProficiencies");
								if (proficienciesNode == null) proficienciesNode = root.AppendChild(document.CreateElement("RacialProficiencies"));
								else proficienciesNode.RemoveAll();

								foreach (string proficiency in race.Proficiencies)
								{
									proficienciesNode.AppendChild(document.CreateElement("Proficiency")).InnerText = proficiency;
								}
							}

							if (race.Languages.Count > 0)
							{
								XmlNode languagesNode = root.SelectSingleNode("RacialLanguages");
								if (languagesNode == null) languagesNode = root.AppendChild(document.CreateElement("RacialLanguages"));
								else languagesNode.RemoveAll();

								foreach (string language in race.Languages)
								{
									languagesNode.AppendChild(document.CreateElement("Language")).InnerText = language;
								}
							}

							if (race.Traits.Count > 0)
							{
								XmlNode traitsNode = root.SelectSingleNode("RacialTraits");
								if (traitsNode == null) traitsNode = root.AppendChild(document.CreateElement("RacialTraits"));
								else traitsNode.RemoveAll();

								foreach (Tuple<string, string> trait in race.Traits)
								{
									XmlNode traitNode = traitsNode.AppendChild(document.CreateElement("Trait"));
									traitNode.InnerText = trait.Item1;
									if (trait.Item2 != "") traitNode.Attributes.Append(document.CreateAttribute("range")).Value = trait.Item2;
								}
							}

							document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
						}
						catch (Exception e)
						{
							reply = $"{userInfo.Mention} Opening file failed. {e.Message}";
						}
					}
					else reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
				}
				else reply = $"{userInfo.Mention} Race `{arguments}` was not found. Try `!listRaces` for a list of available races.";
			}
			else reply = $"{userInfo.Mention} A version must be loaded with the `!loadVersion` command.";
			await ReplyAsync(reply);
		}

		[Command("listRaces"), Summary("Lists the races available in the currently loaded ruleset.")]
		public async Task ListRaces()
		{
			if (RuleSet.Stats != null)
			{
				EmbedBuilder builder = new EmbedBuilder();
				string fieldValue = "";
				List<string> raceNames = RuleSet.Races.Keys.ToList();

				for (int i = 0; i < raceNames.Count; i++)
				{
					fieldValue += $"\r\n{raceNames[i]}";

					for (int j = 0; j < RuleSet.Races[raceNames[i]].SubRaces.Count; j++)
					{
						fieldValue += $"\r\n-{RuleSet.Races[raceNames[i]].SubRaces[j].Name}";
					}
				}
				EmbedFieldBuilder field = new EmbedFieldBuilder();
				builder.AddField(field.WithName("Races").WithValue(fieldValue));
				IUserMessage reply = await ReplyAsync(embed: builder.Build());
				await reply.AddReactionAsync(new Emoji("⬅️"));
				//Task.Run(() => ReactionEvent(reply, Context.User));
			}
			else
			{
				await ReplyAsync($"{Context.User.Mention} A version must be loaded with the `!loadVersion` command.");
			}
		}

		private async Task ReactionEvent(IUserMessage message, IUser user)
		{
			TimeSpan endTime = new TimeSpan(0, 30, 0);
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();
			while (stopWatch.Elapsed < endTime)
			{
				IAsyncEnumerable<IReadOnlyCollection<IUser>> reactedUsers = message.GetReactionUsersAsync(new Emoji("⬅️"), 100);
				bool callingUserReacted = await reactedUsers.All(t => { return t.Where(l => { return l.Id == user.Id; }).Count() > 0; });
				if (callingUserReacted)
				{
					await message.RemoveReactionAsync(new Emoji("⬅️"), user);
				}
			}
			stopWatch.Stop();
		}

		[Command("setClass"), Summary("Sets class of the character.")]
		public async Task SetClass([Remainder, Summary("<race>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = $"{userInfo.Mention} Class set successfully.";

			if (RuleSet.Races != null)
			{
				if (RuleSet.Classes.ContainsKey(arguments))
				{
					if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
					{
						try
						{
							Class chosenClass = RuleSet.Classes[arguments];

							XmlDocument document = new XmlDocument();
							document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
							XmlNode root = document.SelectSingleNode("/Character");
							XmlNode classesNode = root.SelectSingleNode("Classes");
							if (classesNode == null) classesNode = root.AppendChild(document.CreateElement("Classes"));
							else classesNode.RemoveAll();
							XmlNode classNode = classesNode.AppendChild(document.CreateElement("Class"));
							classNode.InnerText = arguments;
							classNode.Attributes.Append(document.CreateAttribute("level")).Value = "1";
							classNode.Attributes.Append(document.CreateAttribute("xp")).Value = "0";

							XmlNode hitDieNode = root.SelectSingleNode("HitDieSize");//document.CreateElement("HitDieSize");
							if (hitDieNode == null)
							{
								hitDieNode = root.AppendChild(document.CreateElement("HitDieSize"));
								hitDieNode.Attributes.Append(document.CreateAttribute("hitDieNumber")).Value = $"{chosenClass.HitDieNumber}";
							}
							hitDieNode.InnerText = chosenClass.HitDieSize.ToString();
							hitDieNode.Attributes["hitDieNumber"].Value = $"{chosenClass.HitDieNumber}";

							if (chosenClass.SavingThrows.Count > 0)
							{
								XmlNode savingThrowsNode = root.SelectSingleNode("ClassSavingThrows");
								if (savingThrowsNode == null) savingThrowsNode = root.AppendChild(document.CreateElement("ClassSavingThrows"));
								else savingThrowsNode.RemoveAll();

								foreach (KeyValuePair<string, string> savingThrow in chosenClass.SavingThrows)
								{
									savingThrowsNode.AppendChild(document.CreateElement(savingThrow.Key)).InnerText = savingThrow.Value;
								}
							}

							if (chosenClass.Proficiencies.Count > 0)
							{
								XmlNode proficienciesNode = root.SelectSingleNode("ClassProficiencies");
								if (proficienciesNode == null) proficienciesNode = root.AppendChild(document.CreateElement("ClassProficiencies"));
								else proficienciesNode.RemoveAll();

								foreach (string proficiency in chosenClass.Proficiencies)
								{
									proficienciesNode.AppendChild(document.CreateElement("Proficiency")).InnerText = proficiency;
								}

								if (chosenClass.ChoosableProficiencies.Count > 0)
								{
									EmbedBuilder builder = new EmbedBuilder();
									string fieldValue = "";

									foreach (string proficiency in chosenClass.ChoosableProficiencies)
									{
										fieldValue += $"\r\n{proficiency}";
									}
									EmbedFieldBuilder field = new EmbedFieldBuilder();
									builder.AddField(field.WithName("Proficiencies").WithValue(fieldValue));

									await Context.Channel.SendMessageAsync($"{chosenClass.Name} has choosable proficiencies. Reply with a comma separated list of {chosenClass.ChoosableProficiencyNumber} proficiencies from the following list.", embed: builder.Build());

									while (true)
									{
										IMessage message = (await Context.Channel.GetMessagesAsync(1).FlattenAsync()).First();
										if (message.Author != userInfo) continue;

										if (message.Content == "Cancel")
										{
											await ReplyAsync($"{Context.User.Mention} Class decision cancelled.");
											return;
										}

										string[] choices = message.Content.Split(',');
										if (choices.Length == chosenClass.ChoosableProficiencyNumber)
										{
											foreach (string choice in choices)
											{
												choice.Trim();
												if (!chosenClass.ChoosableProficiencies.Contains(choice))
												{
													await Context.Channel.SendMessageAsync(@"One of your choices was either misspelled or not in the list. Try again or reply 'Cancel' to cancel class decision.");
													continue;
												}
											}
										}
										else
										{
											await Context.Channel.SendMessageAsync(@"Incorrect number of choices. Try again or reply 'Cancel' to cancel class decision.");
											continue;
										}

										foreach (string proficiency in choices)
										{
											proficienciesNode.AppendChild(document.CreateElement("Proficiency")).InnerText = proficiency;
										}
										break;
									}
								}
							}

							if (chosenClass.Equipment.Count > 0)
							{
								XmlNode equipmentNode = root.SelectSingleNode("Inventory");
								if (equipmentNode == null) equipmentNode = root.AppendChild(document.CreateElement("Inventory"));
								else equipmentNode.RemoveAll();

								foreach (Tuple<Tuple<int, string>, Tuple<int, string>> equip in chosenClass.Equipment)
								{
									if (equip.Item2 == null)
									{
										XmlNode itemNode = equipmentNode.AppendChild(document.CreateElement("Item"));
										XmlAttribute name = itemNode.Attributes.Append(document.CreateAttribute("name"));
										name.Value = equip.Item1.Item2;
										XmlAttribute num = itemNode.Attributes.Append(document.CreateAttribute("num"));
										num.Value = $"{equip.Item1.Item1}";
									}
									else
									{
										Tuple<int, string> choice;

										EmbedBuilder builder = new EmbedBuilder();
										string fieldValue = "";

										fieldValue += $"\r\n{equip.Item1.Item1}x {equip.Item1.Item2}";
										fieldValue += $"\r\n{equip.Item2.Item1}x {equip.Item2.Item2}";

										EmbedFieldBuilder field = new EmbedFieldBuilder();
										builder.AddField(field.WithName("Equipment").WithValue(fieldValue));

										await Context.Channel.SendMessageAsync($"You have a choice of equipment. Reply with '1' or '2' to pick from the following list.", embed: builder.Build());

										while (true)
										{
											IMessage message = (await Context.Channel.GetMessagesAsync(1).FlattenAsync()).First();
											if (message.Author != userInfo) continue;

											if (message.Content == "Cancel")
											{
												await ReplyAsync($"{Context.User.Mention} Class decision cancelled.");
												return;
											}

											if (message.Content == "1") choice = equip.Item1;
											else if (message.Content == "2") choice = equip.Item2;
											else
											{
												await Context.Channel.SendMessageAsync(@"Incorrect choice. Try again or reply 'Cancel' to cancel class decision.");
												continue;
											}

											if (!choice.Item2.StartsWith("Choice"))
											{
												XmlNode itemNode = equipmentNode.AppendChild(document.CreateElement("Item"));
												XmlAttribute name = itemNode.Attributes.Append(document.CreateAttribute("name"));
												name.Value = choice.Item2;
												XmlAttribute num = itemNode.Attributes.Append(document.CreateAttribute("num"));
												num.Value = $"{choice.Item1}";
											}
											else
											{
												await Context.Channel.SendMessageAsync($"You can choose an item matching {choice.Item2}. Simply reply with your choice.");

												while (true)
												{
													message = (await Context.Channel.GetMessagesAsync(1).FlattenAsync()).First();
													if (message.Author != userInfo) continue;

													if (message.Content == "Cancel")
													{
														await ReplyAsync($"{Context.User.Mention} Class decision cancelled.");
														return;
													}

													XmlNode itemNode = equipmentNode.AppendChild(document.CreateElement("Item"));
													XmlAttribute name = itemNode.Attributes.Append(document.CreateAttribute("name"));
													name.Value = message.Content;
													XmlAttribute num = itemNode.Attributes.Append(document.CreateAttribute("num"));
													num.Value = $"{choice.Item1}";
													break;
												}
											}
											break;
										}
									}
								}
							}

							if (chosenClass.SpellSlots.Count > 0)
							{
								XmlNode spellSlotsNode = root.SelectSingleNode("SpellSlots");
								if (spellSlotsNode == null) root.AppendChild(document.CreateElement("SpellSlots"));
								else spellSlotsNode.RemoveAll();

								foreach (Tuple<string, int> slots in chosenClass.SpellSlots[1])
								{
									spellSlotsNode.AppendChild(document.CreateElement(slots.Item1)).InnerText = $"{slots.Item2}";
								}
							}

							if (chosenClass.Traits.Count > 0)
							{
								XmlNode traitsNode = root.SelectSingleNode("ClassTraits");
								if (traitsNode == null) traitsNode = root.AppendChild(document.CreateElement("ClassTraits"));
								else traitsNode.RemoveAll();

								foreach (Tuple<string, Tuple<string, string>> trait in chosenClass.Traits.Where(t => t.Item2 == null))
								{
									traitsNode.AppendChild(document.CreateElement("Trait")).InnerText = trait.Item1;
								}
							}

							document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
						}
						catch (Exception e)
						{
							reply = $"{userInfo.Mention} Opening file failed. {e.StackTrace}";
						}
					}
					else reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
				}
				else reply = $"{userInfo.Mention} Class `{arguments}` was not found. Try `!listClasses` for a list of available classes.";
			}
			else reply = $"{Context.User.Mention} A version must be loaded with the `!loadVersion` command.";
			await ReplyAsync(reply);
		}

		[Command("listClasses"), Summary("Lists the classes available in the currently loaded ruleset.")]
		public async Task ListClasses()
		{
			if (RuleSet.Stats != null)
			{
				EmbedBuilder builder = new EmbedBuilder();
				string fieldValue = "";
				List<string> classNames = RuleSet.Classes.Keys.ToList();

				foreach (string name in classNames)
				{
					fieldValue += $"\r\n{name}";
				}
				EmbedFieldBuilder field = new EmbedFieldBuilder();
				builder.AddField(field.WithName("Classes").WithValue(fieldValue));
				await ReplyAsync(embed: builder.Build());
			}
			else
			{
				await ReplyAsync($"{Context.User.Mention} A version must be loaded with the `!loadVersion` command.");
			}
		}

		[Command("addItem"), Summary("Add an item to character's inventory. Item number separated by '|'.")]
		public async Task AddItem([Remainder, Summary("<item name>|<num>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNode root = document.SelectSingleNode("/Character");
				XmlNode inventoryNode = root.SelectSingleNode("/Inventory");

				if (inventoryNode == null)
				{
					inventoryNode = root.AppendChild(document.CreateElement("Inventory"));
				}

				string[] itemInfo = arguments.Split('|');

				foreach (XmlNode itemNode in inventoryNode)
				{
					if (itemNode.Attributes["name"].Value.ToLower() == itemInfo[0].ToLower())
					{
						itemNode.Attributes["num"].Value = (int.Parse(itemNode.Attributes["num"].Value) + ((itemInfo.Length > 1) ? int.Parse(itemInfo[1]) : 1)).ToString();
						reply = $"{itemInfo[0]} was added successfully.";
						break;
					}
				}

				if (reply == "")
				{
					XmlNode itemNode = inventoryNode.AppendChild(document.CreateElement("Item"));
					itemNode.Attributes.Append(document.CreateAttribute("name")).Value = itemInfo[0];
					itemNode.Attributes.Append(document.CreateAttribute("num")).Value = (itemInfo.Length > 1) ? itemInfo[1] : "1";
					reply = $"{itemInfo[0]} was added successfully.";
				}
				document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
			}
			else
			{
				reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
			}
			await ReplyAsync(reply);
		}

		[Command("removeItem"), Summary("Remove an item from character's inventory. Item number separated by '|'.")]
		public async Task RemoveItem([Remainder, Summary("<item name>|<num>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNodeList itemNodes = document.SelectNodes("/Character/Inventory/Item");

				if (itemNodes.Count == 0)
				{
					await ReplyAsync("Selected character has no items in inventory.");
					return;
				}

				string[] itemInfo = arguments.Split('|');

				foreach (XmlNode itemNode in itemNodes)
				{
					if (itemNode.Attributes["name"].Value.ToLower() == itemInfo[0].ToLower())
					{
						int itemNum = (itemInfo.Length > 1) ? int.Parse(itemInfo[1]) : 1;
						if (int.Parse(itemNode.Attributes["num"].Value) < itemNum)
						{
							reply = $"Character does not have {itemNum} of {itemInfo[0]}.";
							break;
						}
						else if (int.Parse(itemNode.Attributes["num"].Value) == itemNum)
						{
							itemNode.ParentNode.RemoveChild(itemNode);
						}
						else if (int.Parse(itemNode.Attributes["num"].Value) < itemNum && itemNum > 0)
						{
							itemNode.Attributes["num"].Value = (int.Parse(itemNode.Attributes["num"].Value) - itemNum).ToString();
						}
						reply = $"{itemNum} of {itemInfo[0]} removed from inventory.";
						break;
					}
				}
				document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
			}
			else
			{
				reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
			}
			await ReplyAsync(reply);
		}

		[Command("viewInventory"), Summary("Displays character's inventory.")]
		public async Task DisplayInventory()
		{
			var userInfo = Context.User;
			string inventoryString = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNodeList itemNodes = document.SelectNodes("/Character/Inventory/Item");

				if (itemNodes.Count == 0)
				{
					await ReplyAsync("Selected character has no items in inventory.");
					return;
				}

				foreach (XmlNode itemNode in itemNodes)
				{
					inventoryString += $"{itemNode.Attributes["num"].Value}x {itemNode.Attributes["name"].Value}\r\n";
				}

				EmbedBuilder embed = new EmbedBuilder();
				embed.Color = Color.Purple;

				EmbedFieldBuilder inventoryField = new EmbedFieldBuilder();
				inventoryField.Name = $"{DataMemory.selectedCharacters[userInfo.Id]}'s Inventory";
				inventoryField.Value = inventoryString;
				embed.AddField(inventoryField);

				await ReplyAsync(embed: embed.Build());
			}
			else
			{
				await ReplyAsync($"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.");
			}
		}

		[Command("addSpell"), Summary("Adds a spell to character's spell list/book. An extra piece of info can be stored by being separated with a '|' and the info type and data can be separated with a ':'.")]
		public async Task AddSpell([Remainder, Summary("<spell name>|<<info type>:<info data>>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNode root = document.SelectSingleNode("/Character");
				XmlNode spellsNode = root.SelectSingleNode("/Spells");

				if (spellsNode == null)
				{
					spellsNode = root.AppendChild(document.CreateElement("Spells"));
				}

				string[] spellInfo = arguments.Split('|');
				string[] extraSpellInfo = (spellInfo.Length > 1) ? spellInfo[1].Split(':') : null;
				string extraSpellInfoType = "";

				if (extraSpellInfo != null)
				{
					foreach (string i in extraSpellInfo[0].Split(' '))
					{
						if (extraSpellInfoType == "")
						{
							extraSpellInfoType += i.Substring(0, 1).ToLower() + i.Substring(1);
						}
						else
						{
							extraSpellInfoType += i.Substring(0, 1).ToUpper() + i.Substring(1);
						}
					}
				}

				foreach (XmlNode loopSpellNode in spellsNode)
				{
					if (loopSpellNode.Attributes.Count == spellInfo.Length && loopSpellNode.Attributes["name"].Value.ToLower() == spellInfo[0].ToLower())
					{
						if (loopSpellNode.Attributes[extraSpellInfoType] != null)
						{
							if (loopSpellNode.Attributes[extraSpellInfoType].Value.ToLower() == extraSpellInfo[1].ToLower())
							{
								reply = $"{spellInfo[0]} already exists.";
								break;
							}
						}
						else if (loopSpellNode.Attributes.Count == 1)
						{
							reply = $"{spellInfo[0]} already exists.";
							break;
						}
					}
				}

				if (reply == "")
				{
					XmlNode spellNode = spellsNode.AppendChild(document.CreateElement("Spell"));
					spellNode.Attributes.Append(document.CreateAttribute("name")).Value = spellInfo[0];
					if (extraSpellInfo != null) spellNode.Attributes.Append(document.CreateAttribute(extraSpellInfoType)).Value = extraSpellInfo[1];
					reply = $"{spellInfo[0]} added to character successfully.";
				}
				document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
			}
			else
			{
				reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
			}
			await ReplyAsync(reply);
		}

		[Command("removeSpell"), Summary("Remove a spell from character's spell list/book. Include extra spell info separated from the name with a '|' with the info type and data separated by a ':'. Leave blank to be given an option of spells to delete if more than one by the given name exists.")]
		public async Task RemoveSpell([Remainder, Summary("<spell name>|<<info type>:<info data>>")] string arguments)
		{
			var userInfo = Context.User;
			string reply = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNodeList spellNodes = document.SelectNodes("/Character/Spells/Spell");

				string[] spellInfo = arguments.Split('|');
				string[] extraSpellInfo = (spellInfo.Length > 1) ? spellInfo[1].Split(':') : null;
				string extraSpellInfoType = "";

				if (extraSpellInfo != null)
				{
					foreach (string i in extraSpellInfo[0].Split(' '))
					{
						if (extraSpellInfoType == "")
						{
							extraSpellInfoType += i.Substring(0, 1).ToLower() + i.Substring(1);
						}
						else
						{
							extraSpellInfoType += i.Substring(0, 1).ToUpper() + i.Substring(1);
						}
					}
				}

				List<XmlNode> matchingSpells = new List<XmlNode>();

				foreach (XmlNode spellNode in spellNodes)
				{
					if (spellNode.Attributes.Count == spellInfo.Length && spellNode.Attributes["name"].Value.ToLower() == spellInfo[0].ToLower())
					{
						if (spellNode.Attributes[extraSpellInfoType] != null)
						{
							if (spellNode.Attributes[extraSpellInfoType].Value.ToLower() == extraSpellInfo[1].ToLower())
							{
								spellNode.ParentNode.RemoveChild(spellNode);
								reply = $"{spellInfo[0]} successfully removed.";
								break;
							}
						}
						else if (spellNode.Attributes.Count == 1)
						{
							spellNode.ParentNode.RemoveChild(spellNode);
							reply = $"{spellInfo[0]} successfully removed.";
							break;
						}
					}
					else if (spellNode.Attributes.Count > spellInfo.Length && spellNode.Attributes["name"].Value.ToLower() == spellInfo[0].ToLower())
					{
						matchingSpells.Add(spellNode);
					}
				}

				if (reply == "")
				{
					if (matchingSpells.Count > 1)
					{
						EmbedBuilder builder = new EmbedBuilder();
						builder.Color = Color.Purple;
						EmbedFieldBuilder matchingSpellsField = new EmbedFieldBuilder();
						matchingSpellsField.Name = "Matching Spells";

						string matchingSpellsString = "";
						foreach (XmlNode spellNode in matchingSpells)
						{
							string temp = (spellNode.Attributes.Count > 1) ? $" - {spellNode.Attributes[1].Name} : {spellNode.Attributes[1].Value}" : "";
							matchingSpellsString += $"{spellNode.Attributes["name"].Value}{temp}\r\n";
						}
						matchingSpellsField.Value = matchingSpellsString;

						await Context.Channel.SendMessageAsync($"Found multiple spells that match {spellInfo[0]}. Respond with line number of spell to remove from following list or with 'ALL' to remove them all. Responding with anything else will cancel the command.", embed: builder.Build());

						while (true)
						{
							IMessage message = (await Context.Channel.GetMessagesAsync(1).FlattenAsync()).First();
							if (message.Author != userInfo) continue;

							if (message.Content == "ALL")
							{
								matchingSpells.ForEach(t => t.ParentNode.RemoveChild(t));
								reply = $"All spells matching {spellInfo[0]} successfully removed.";
								break;
							}

							int spellNum;
							if (int.TryParse(message.Content, out spellNum))
							{
								if (spellNum - 1 < matchingSpells.Count)
								{
									matchingSpells[spellNum - 1].ParentNode.RemoveChild(matchingSpells[spellNum - 1]);
									reply = "Requested spell deleted.";
									break;
								}
								else
								{
									await ReplyAsync("Requested spell number out of bounds. Cancelling command.");
									return;
								}
							}

							await ReplyAsync("Responded with something unexpected. Cancelling command.");
							return;
						}
					}
					else
					{
						matchingSpells[0].ParentNode.RemoveChild(matchingSpells[0]);
						reply = $"Spell matching {spellInfo[0]} successfully removed.";
					}
				}
				document.Save($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
			}
			else
			{
				reply = $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
			}
			await ReplyAsync(reply);
		}

		[Command("viewSpells"), Summary("Displays character's spells.")]
		public async Task DisplaySpells()
		{
			var userInfo = Context.User;
			string spellsString = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNode spellsNode = document.SelectSingleNode("/Character/Spells");

				if (spellsNode == null)
				{
					await ReplyAsync($"Character has no spells to display.");
					return;
				}

				foreach (XmlNode spellNode in spellsNode)
				{
					string temp = (spellNode.Attributes.Count > 1) ? $" - {spellNode.Attributes[1].Name} : {spellNode.Attributes[1].Value}" : "";
					spellsString += $"{spellNode.Attributes["name"].Value}{temp}\r\n";
				}

				EmbedBuilder embed = new EmbedBuilder();
				embed.Color = Color.Purple;

				EmbedFieldBuilder spellsField = new EmbedFieldBuilder();
				spellsField.Name = $"{DataMemory.selectedCharacters[userInfo.Id]}'s Spells";
				spellsField.Value = spellsString;
				embed.AddField(spellsField);

				await ReplyAsync(embed: embed.Build());
			}
			else
			{
				await ReplyAsync($"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.");
			}
		}

		[Command("viewStats"), Summary("Displays character's stats.")]
		public async Task DisplayStats()
		{
			var userInfo = Context.User;
			string statsValue = "";

			if (DataMemory.selectedCharacters.ContainsKey(userInfo.Id))
			{
				XmlDocument document = new XmlDocument();

				document.Load($"Characters/{userInfo.Id}/{DataMemory.selectedCharacters[userInfo.Id]}.xml");
				XmlNode statsNode = document.SelectSingleNode("/Character/Stats");

				if (statsNode != null)
				{
					foreach (XmlNode stat in statsNode.ChildNodes)
					{
						statsValue += $"{stat.LocalName}: {stat.InnerText}\r\n";
					}

					EmbedBuilder embed = new EmbedBuilder();
					embed.Color = Color.Purple;

					EmbedFieldBuilder statsField = new EmbedFieldBuilder();
					statsField.Name = "Stats:";

					statsField.Value = statsValue;
					embed.AddField(statsField);

					await ReplyAsync(embed: embed.WithAuthor(document.ChildNodes[0].Attributes[0].Value).Build());
				}
				else
				{
					statsValue += $"{userInfo.Mention} It appears that your character has no stats. Please run `!rollStats`";
					await ReplyAsync(statsValue);
				}
			}
			else
			{
				statsValue += $"{userInfo.Mention} A character must be selected for use. Either select a character using `!select` or create a new character.";
				await ReplyAsync(statsValue);
			}
		}
	}
}

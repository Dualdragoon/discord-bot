﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_Bot
{
	struct Race
	{
		public Dictionary<string, string> SavingThrows { get; set; }
		public Dictionary<string, int> StatIncreases { get; set; }

		public int Speed { get; set; }

		public List<string> Languages { get; set; }
		public List<string> Proficiencies { get; set; }
		public List<string> Resistances { get; set; }
		public List<Tuple<string, string>> Traits { get; set; }

		public List<Race> SubRaces { get; set; }

		public string Name { get; set; }
		public string Size { get; set; }

		public Race(string name)
		{
			SavingThrows = new Dictionary<string, string>();
			StatIncreases = new Dictionary<string, int>();

			Languages = new List<string>();
			Proficiencies = new List<string>();
			Resistances = new List<string>();
			Traits = new List<Tuple<string, string>>();

			SubRaces = new List<Race>();

			Speed = 0;
			Name = name;
			Size = "";
		}
	}
}

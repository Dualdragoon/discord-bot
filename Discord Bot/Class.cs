﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Discord_Bot
{
	struct Class
	{
		public Dictionary<string, string> SavingThrows { get; set; }

		public List<string> ChoosableProficiencies { get; set; }
		public List<string> Proficiencies { get; set; }
		public List<Tuple<Tuple<int, string>, Tuple<int, string>>> Equipment { get; set; }
		public Dictionary<int, List<Tuple<string, int>>> SpellSlots { get; set; }
		public List<Tuple<string, Tuple<string, string>>> Traits { get; set; }

		public int ChoosableProficiencyNumber { get; set; }
		public int HitDieNumber { get; set; }
		public int HitDieSize { get; set; }
		public string Name { get; set; }

		public Class(string name)
		{
			SavingThrows = new Dictionary<string, string>();

			ChoosableProficiencies = new List<string>();
			Proficiencies = new List<string>();
			Equipment = new List<Tuple<Tuple<int, string>, Tuple<int, string>>>();
			SpellSlots = new Dictionary<int, List<Tuple<string, int>>>();
			Traits = new List<Tuple<string, Tuple<string, string>>>();

			ChoosableProficiencyNumber = 0;
			HitDieNumber = 0;
			HitDieSize = 0;
			Name = name;
		}
	}
}

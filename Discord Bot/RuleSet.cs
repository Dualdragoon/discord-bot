﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Discord_Bot
{
	static class RuleSet
	{
		public static int statRolls = 4, topRolls = 3, statDie = 6;
		public static string name = "No loaded ruleset";
		public static List<string> Stats { get; private set; }

		public static Dictionary<string, Class> Classes { get; private set; }
		public static Dictionary<string, Race> Races { get; private set; }
		public static Dictionary<string, Race> RacesAndSubraces { get; private set; }

		public static void LoadRuleSet(string ruleSetName)
		{
			name = ruleSetName;
			XmlDocument document = new XmlDocument();
			document.Load($"Versions/{ruleSetName}.xml");

			XmlNode statRulesNode = document.SelectSingleNode("/Root/StatRules");
			Stats = statRulesNode.Attributes[0].Value.Split(' ').ToList();
			int.TryParse(statRulesNode.SelectSingleNode("Roll").InnerText, out statRolls);
			int.TryParse(statRulesNode.SelectSingleNode("Take").InnerText, out topRolls);
			int.TryParse(statRulesNode.SelectSingleNode("Die").InnerText, out statDie);

			Races = new Dictionary<string, Race>();
			RacesAndSubraces = new Dictionary<string, Race>();
			XmlNodeList raceNodes = document.SelectNodes("/Root/Races/Race");
			foreach (XmlNode raceNode in raceNodes)
			{
				Race race = new Race(raceNode.Attributes[0].Value);

				RaceLoader(ref race, raceNode);

				Races.Add(race.Name, race);
				RacesAndSubraces.Add(race.Name, race);

				foreach (XmlNode subRaceNode in raceNode.SelectNodes("Subraces/Subrace"))
				{
					Race subRace = race;

					RaceLoader(ref subRace, subRaceNode);

					race.SubRaces.Add(subRace);
					RacesAndSubraces.Add(subRace.Name, subRace);
				}
			}

			Classes = new Dictionary<string, Class>();
			XmlNodeList classNodes = document.SelectNodes("/Root/Classes/Class");
			foreach (XmlNode classNode in classNodes)
			{
				Class gameClass = new Class(classNode.Attributes[0].Value);

				ClassLoader(ref gameClass, classNode);

				Classes.Add(gameClass.Name, gameClass);
			}
		}

		private static void RaceLoader(ref Race race, XmlNode raceNode)
		{
			race.Name = raceNode.Attributes[0].Value;
			if (raceNode.SelectSingleNode("Size") != null)
				race.Size = raceNode.SelectSingleNode("Size").InnerText;
			if (raceNode.SelectSingleNode("Speed") != null)
				race.Speed = int.Parse(raceNode.SelectSingleNode("Speed").InnerText);

			XmlNode currentNode = raceNode.SelectSingleNode("StatIncreases");
			if (currentNode != null)
				foreach (XmlNode statIncrease in currentNode)
					race.StatIncreases.Add(statIncrease.Name, int.Parse(statIncrease.InnerText));

			currentNode = raceNode.SelectSingleNode("SavingThrows");
			if (currentNode != null)
				foreach (XmlNode savingThrow in currentNode)
					race.SavingThrows.Add(savingThrow.Name, savingThrow.InnerText);

			currentNode = raceNode.SelectSingleNode("Resistances");
			if (currentNode != null)
				foreach (XmlNode resistance in currentNode)
					race.Resistances.Add(resistance.InnerText);

			currentNode = raceNode.SelectSingleNode("Proficiencies");
			if (currentNode != null)
				foreach (XmlNode proficiency in currentNode)
					race.Proficiencies.Add(proficiency.InnerText);

			currentNode = raceNode.SelectSingleNode("Languages");
			if (currentNode != null)
				foreach (XmlNode language in currentNode)
					race.Languages.Add(language.InnerText);

			currentNode = raceNode.SelectSingleNode("Traits");
			if (currentNode != null)
				foreach (XmlNode trait in currentNode)
					race.Traits.Add(Tuple.Create(trait.InnerText, (trait.Attributes.Count > 0) ? trait.Attributes[0].Value : ""));
		}

		private static void ClassLoader(ref Class gameClass, XmlNode classNode)
		{
			gameClass.Name = classNode.Attributes[0].Value;
			if (classNode.SelectSingleNode("HitDieNumber") != null)
				gameClass.HitDieNumber = int.Parse(classNode.SelectSingleNode("HitDieNumber").InnerText);
			if (classNode.SelectSingleNode("HitDieSize") != null)
				gameClass.HitDieSize = int.Parse(classNode.SelectSingleNode("HitDieSize").InnerText);

			XmlNode currentNode = classNode.SelectSingleNode("SavingThrows");
			if (currentNode != null)
				foreach (XmlNode savingThrow in currentNode)
					gameClass.SavingThrows.Add(savingThrow.Name, savingThrow.InnerText);

			currentNode = classNode.SelectSingleNode("Proficiencies");
			if (currentNode != null)
				foreach (XmlNode proficiency in currentNode)
					gameClass.Proficiencies.Add(proficiency.InnerText);

			currentNode = classNode.SelectSingleNode("ChoosableProficiencies");
			if (currentNode != null)
			{
				gameClass.ChoosableProficiencyNumber = int.Parse(currentNode.Attributes[0].Value);
				foreach (XmlNode choosableProficiency in currentNode)
					gameClass.ChoosableProficiencies.Add(choosableProficiency.InnerText);
			}

			currentNode = classNode.SelectSingleNode("Equipment");
			if (currentNode != null)
			{
				foreach (XmlNode equipment in currentNode)
				{
					int number = 1, numberAlternate = 1;
					string alternate = "";

					foreach (XmlAttribute attribute in equipment.Attributes)
					{
						switch (attribute.Name)
						{
							case "alternative":
								alternate = attribute.InnerText;
								break;

							case "num":
								number = int.Parse(attribute.InnerText);
								break;

							case "altNum":
								numberAlternate = int.Parse(attribute.InnerText);
								break;

							default:
								break;
						}
					}
					gameClass.Equipment.Add(Tuple.Create(
						Tuple.Create(number, equipment.InnerText),
						(alternate != "") ? Tuple.Create(numberAlternate, alternate) : null));
				}
			}

			currentNode = classNode.SelectSingleNode("SpellSlots");
			if (currentNode != null)
			{
				foreach (XmlNode levelNode in currentNode)
				{
					gameClass.SpellSlots.Add(int.Parse(levelNode.Name.Replace("Level", "")), new List<Tuple<string, int>>());
					foreach (XmlNode choice in levelNode)
						gameClass.SpellSlots[int.Parse(levelNode.Name.Replace("Level", ""))].Add(Tuple.Create(choice.Name, int.Parse(choice.InnerText)));
				}
			}

			currentNode = classNode.SelectSingleNode("Traits");
			if (currentNode != null)
				foreach (XmlNode trait in currentNode)
					gameClass.Traits.Add(Tuple.Create(trait.InnerText, (trait.Attributes.Count > 0) ? Tuple.Create(trait.Attributes[0].Name, trait.Attributes[0].InnerText) : null));
		}
	}
}
